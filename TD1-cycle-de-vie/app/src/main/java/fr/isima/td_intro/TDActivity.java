package fr.isima.td_intro;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

public abstract class TDActivity extends Activity implements View.OnClickListener {
    private String tag;
    private int layoutId;

    protected Button openActivity;
    protected CheckBox clearTop;
    protected CheckBox singleTop;


    public TDActivity(String tag, int layoutId) {
        this.tag = tag;
        this.layoutId = layoutId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(tag, tag + " : onCreate");
        setContentView(layoutId);

        openActivity = (Button) findViewById(R.id.openActivity);
        clearTop = (CheckBox) findViewById(R.id.clearTop);
        singleTop = (CheckBox) findViewById(R.id.singleTop);

        openActivity.setOnClickListener(this);
        clearTop.setOnClickListener(this);
        singleTop.setOnClickListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(tag, tag + " : onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(tag, tag + " : onResume");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(tag, tag + " : onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(tag, tag + " : onRestart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(tag, tag + " : onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(tag, tag + " : onDestroy");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.i(tag, tag + " : onNewIntent");
    }

    @Override
    public void onClick(View view) {
        if (view == openActivity) {
            Log.i(tag, tag + " : clic sur 'ouvrir Activity'");
            startActivity(getIntent(false));
        }
        else {
            Integer flags = 0;

            if (view == clearTop) {
                Log.i(tag, tag + " : flag clear top " + (clearTop.isChecked() ? "ON" : "OFF"));
            }
            else {
                Log.i(tag, tag + " : flag single top " + (singleTop.isChecked() ? "ON" : "OFF"));
            }

            if (clearTop.isChecked()) {
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP;
            }
            if (singleTop.isChecked()) {
                flags |= Intent.FLAG_ACTIVITY_SINGLE_TOP;
            }

            if (flags > 0) {
                getIntent(false).setFlags(flags);
            }
            else {
                getIntent(true);
            }
        }
    }

    protected abstract Intent getIntent(boolean forceNew);
}
