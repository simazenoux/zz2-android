package fr.isima.td_intro;

import android.content.Intent;

public class ActivityOne extends TDActivity {
    private static final String TAG = ActivityOne.class.getName();

    private Intent activityTwoIntent;


    public ActivityOne() {
        super(TAG, R.layout.activity_one);
    }

    @Override
    protected Intent getIntent(boolean forceNew) {
        if (forceNew || activityTwoIntent == null) {
            activityTwoIntent = new Intent(this, ActivityTwo.class);
        }

        return activityTwoIntent;
    }
}
