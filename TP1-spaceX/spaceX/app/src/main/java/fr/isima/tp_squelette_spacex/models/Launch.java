package fr.isima.tp_squelette_spacex.models;

import java.io.Serializable;

public class Launch implements Serializable {

    public String mission_name;
    public String launch_date_unix;
    public LaunchRocket rocket;
    public LaunchLinks links;
}
