package fr.isima.tp_squelette_spacex.ws;

import java.util.List;

// import fr.isima.tp_squelette_spacex.models.Launch;
import fr.isima.tp_squelette_spacex.models.Launch;
import fr.isima.tp_squelette_spacex.models.Rocket;
import retrofit2.Call;
import retrofit2.http.GET;

public interface SpaceXService {

    @GET("launches")
    Call<List<Launch>> listLaunches();

    @GET("rockets")
    Call<List<Rocket>> listRockets();
}
