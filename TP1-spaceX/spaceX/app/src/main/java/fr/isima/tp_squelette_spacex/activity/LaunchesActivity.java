package fr.isima.tp_squelette_spacex.activity;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import fr.isima.tp_squelette_spacex.R;
import fr.isima.tp_squelette_spacex.adapter.LaunchAdapter;
import fr.isima.tp_squelette_spacex.models.Launch;
import fr.isima.tp_squelette_spacex.ws.WsManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LaunchesActivity extends Activity implements AdapterView.OnItemClickListener, Callback<List<Launch>> {

    private ListView listView;
    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i("LaunchesActivity", "LaunchesActivity" + " : onCreate");
        setContentView(R.layout.activity_launches);

        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        listView = (ListView) findViewById(R.id.listview);

        listView.setOnItemClickListener(this);
        loadLaunches();
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        LaunchAdapter launchAdapter = (LaunchAdapter) listView.getAdapter();
        Launch launch = launchAdapter.getItem(position);


        if (launch.links.article_link.equals(""))
        {
            Context context = getApplicationContext();
            CharSequence text = "Pas de lien vers cette page !";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
        else
        {
            Intent myIntent;
            if (launch.links.article_link.startsWith("https"))
            {
                myIntent = new Intent(this, LaunchActivity.class).putExtra(String.valueOf(R.string.launch),launch);
            }
            else
            {
                myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(launch.links.article_link));
            }

            startActivity(myIntent);
        }

    }


    public void loadLaunches() {
        progressBar.setVisibility(View.VISIBLE);
        Call<List<Launch>> launches = WsManager.getSpaceXService().listLaunches();

        launches.enqueue(this);
    }

    @Override
    public void onResponse(Call<List<Launch>> call, Response<List<Launch>> response) {
        progressBar.setVisibility(View.INVISIBLE);
        LaunchAdapter launchAdapter = new LaunchAdapter(this, R.layout.item_launches, response.body());
        listView.setAdapter(launchAdapter);
    }

    @Override
    public void onFailure(Call<List<Launch>> call, Throwable t) {
        progressBar.setVisibility(View.INVISIBLE);

        Toast toast = Toast.makeText(getApplicationContext(), R.string.loading_error, Toast.LENGTH_SHORT);
        toast.show();
    }
}
