package fr.isima.tp_squelette_spacex.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import androidx.viewpager.widget.ViewPager;

import fr.isima.tp_squelette_spacex.R;
import fr.isima.tp_squelette_spacex.adapter.PicturesPager;
import fr.isima.tp_squelette_spacex.models.Rocket;

public class RocketActivity extends Activity {

    private Rocket rocket;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i("LaunchActivity", "LaunchActivity" + " : onCreate");
        setContentView(R.layout.activity_rocket);

        rocket = (Rocket) getIntent().getExtras().get(String.valueOf(R.string.rocket));

        if (rocket.flickr_images.isEmpty())
        {
            finish();
        }
        else
        {
            viewPager = (ViewPager) findViewById(R.id.viewpager);
            viewPager.setAdapter(new PicturesPager(this, rocket.flickr_images));
        }
    }
}
