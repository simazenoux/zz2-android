package fr.isima.tp_squelette_spacex.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import fr.isima.tp_squelette_spacex.R;
import fr.isima.tp_squelette_spacex.models.Launch;

public class LaunchActivity extends Activity {

    private Launch launch;
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i("LaunchActivity", "LaunchActivity" + " : onCreate");
        setContentView(R.layout.activity_launch);

        launch = (Launch) getIntent().getExtras().get(String.valueOf(R.string.launch));

        webView = (WebView) findViewById(R.id.webview);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(launch.links.article_link);
    }
}
