package fr.isima.tp_squelette_spacex.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import fr.isima.tp_squelette_spacex.R;
import fr.isima.tp_squelette_spacex.models.Launch;

public class LaunchAdapter extends ArrayAdapter<Launch> {

    private LayoutInflater layoutInflater;
    private int layoutResourceId;

    public LaunchAdapter(Activity activity, int layoutResourceId, List<Launch> objects) {
        super(activity, layoutResourceId, objects);

        this.layoutResourceId = layoutResourceId;
        this.layoutInflater = activity.getLayoutInflater();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View view = convertView;

        if (view == null)
        {
            view = layoutInflater.inflate(R.layout.item_launches, parent, false);
        }

        ViewHolder holder = (ViewHolder)view.getTag();
        if (holder == null)
        {
            holder = new ViewHolder(view);
            view.setTag(holder);
        }
        Launch launch = getItem(position);
        holder.missionName.setText(launch.mission_name);
        holder.rocketName.setText(launch.rocket.rocket_name);
        holder.launchDate.setText(launch.launch_date_unix);

        return view;
    }

    private class ViewHolder {
        public TextView missionName;
        public TextView rocketName;
        public TextView launchDate;

        public ViewHolder(View row) {
            missionName = row.findViewById(R.id.mission_name);
            rocketName = row.findViewById(R.id.rocket_name);
            launchDate = row.findViewById(R.id.launch_date);
        }
    }
}
