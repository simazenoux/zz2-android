package fr.isima.tp_squelette_spacex.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import fr.isima.tp_squelette_spacex.R;
import fr.isima.tp_squelette_spacex.adapter.RocketAdapter;
import fr.isima.tp_squelette_spacex.models.Rocket;
import fr.isima.tp_squelette_spacex.ws.WsManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RocketsActivity extends Activity implements AdapterView.OnItemClickListener, Callback<List<Rocket>> {

    private ListView listView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i("LaunchesActivity", "LaunchesActivity" + " : onCreate");
        setContentView(R.layout.activity_launches);

        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        listView = (ListView) findViewById(R.id.listview);

        listView.setOnItemClickListener(this);
        loadRockets();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        RocketAdapter rocketAdapter = (RocketAdapter) listView.getAdapter();
        Rocket rocket = rocketAdapter.getItem(position);

        Intent myIntent = new Intent(this, RocketActivity.class).putExtra(String.valueOf(R.string.rocket), rocket);
        startActivity(myIntent);
    }

    public void loadRockets() {
        progressBar.setVisibility(View.VISIBLE);
        Call<List<Rocket>> rockets = WsManager.getSpaceXService().listRockets();

        rockets.enqueue(this);
    }

    @Override
    public void onResponse(Call<List<Rocket>> call, Response<List<Rocket>> response) {
        progressBar.setVisibility(View.INVISIBLE);
        RocketAdapter rocketAdapter = new RocketAdapter(this, R.layout.item_rockets, response.body());
        listView.setAdapter(rocketAdapter);
    }

    @Override
    public void onFailure(Call<List<Rocket>> call, Throwable t) {
        progressBar.setVisibility(View.INVISIBLE);

        Toast toast = Toast.makeText(getApplicationContext(), R.string.loading_error, Toast.LENGTH_SHORT);
        toast.show();
    }


}
